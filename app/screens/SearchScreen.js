import React from 'react';
//import { Platform } from 'react-native';
import {
  ScrollView,
  StyleSheet,
  TextInput,
  View,
  Picker,
  Text,
  Modal,
  TouchableHighlight,
  Alert } from 'react-native';
import { Card, ListItem, Overlay, SearchBar } from 'react-native-elements'
import Swipeable from 'react-native-swipeable';
import { WebBrowser, Icon } from 'expo';
import TabBarIcon from '../components/TabBarIcon';

const leftContent = <Text>ICON <TabBarIcon name={'md-heart'}/></Text>

export default class LinksScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { text: '', placeholder: "Enter city here", items: [], isVisible: false };
    //this.probs = { items: []}
  }

  async componentDidMount() {
    await this._searchApartments("oulu")
  }

  static navigationOptions = {
    title: 'Search apartments',
    //header: null,
    headerStyle: {
      backgroundColor: '#4caf50',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{flexDirection:'row', flexWrap:'wrap', marginBottom: 5}}> 
        {/*
          <TextInput
            style={{flex: 0.9, height: 40, marginLeft: 10, padding: 10, borderColor: '#eeeeee', borderWidth: 1, borderRadius: 10}}
            onChangeText={(text) => this.setState({text})}
            value={this.state.text}
            placeholder={this.state.placeholder}
          />
          <Button
            style={{flex: 0.1, color: "white", padding: 0}}
            iconRight={{
              name: "search",
              size: 18,
              color: 'white'
            }}
            backgroundColor="#4caf50"
            onPress={this._searchApartments}
            accessibilityLabel="Learn more about this purple button"_handlePressSearch
          />
          */}
          <SearchBar containerStyle={{flex: 1, backgroundColor: "#fff", borderBottomWidth: 0}}
            inputStyle={{backgroundColor: "#eee"}}
            lightTheme
            round
            onChangeText={this._searchApartments}
            onClearText={this._searchApartments}
            placeholder='Enter city here...'
          />
        </View>
    
          <Card containerStyle={{paddingBottom: 5, margin: 0}} >
            {
              this.state.items.map((item, i) => {
                const title = `${item.area}, ${item.address}, ${item.rent}, ${item.m2}`;
                
                let subtitle;
                if (item.mod) {
                  subtitle = `${item.mod.replace('tänään', '')}: ${item.desc}`;
                } else {
                  subtitle = item.desc;
                }
                return (
                  <Swipeable
                    key={i}
                    leftContent={leftContent}>
                    <ListItem
                      key={i}
                      title={title}
                      subtitle={subtitle}
                      //avatar={{uri:item.rent}}
                      onPress={this._goToApartmentLink.bind(this, i)}
                      onLongPress={this._showOptions}
                    />
                  </Swipeable>
                );
              })
            }
          </Card>
      </ScrollView>
    );
  }

  _searchApartments = async (searchTerm = null) => {
    if (!searchTerm) {
      searchTerm = this.state.text;
    }
    if (!searchTerm.length) return false;
    searchTerm = searchTerm.toLowerCase();
    const searchUrl = `https://find-apartment.herokuapp.com/api/get_rent_data?q=${searchTerm}`
    try {
      const response = await fetch(searchUrl);
      let responseJson = await response.json();
      this.setState(previousState => (
        { items: responseJson.items }
      ))
      return responseJson.items;
    } catch (error) {
      console.error(error);
    }
    return [];
  };

  _showOptions = (visible) => {
    this.setState({isVisible: !this.state.isVisible});
  }

  _goToApartmentLink = async(index) => {
    const link = this.state.items[index].link
    return WebBrowser.openBrowserAsync(link);
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    //backgroundColor: '#ebedf1',
  },
});
