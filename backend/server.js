const request = require('request');
const express = require('express');
const cheerio = require('cheerio')

const app = express();

const magic = "TAIKASANA";

const LRU = require("lru-cache")
  , options = { max: 5000
              , length: function (n, key) { return n * 2 + key.length }
              , dispose: function (key, n) { }
              , maxAge: 1000 * 60 * 60 }
  , cache = new LRU(options);

const urlMap = {
    tori: "https://www.tori.fi/pohjois-pohjanmaa?q=TAIKASANA&cg=1010&w=1&st=u&at=&ht=&mre=&ros=&roe=&ss=&se=&ca=2&l=0&md=th",
    etuovi: "https://asunnot.oikotie.fi/vuokrattavat-asunnot?locations=%5B%5B274,6,%22TAIKASANA%22%5D%5D",
    ta: "https://ta.fi/vapaat-asunnot/asunnot/kaikki-am/kaikki-kunnat/kaikki-alueet?kunnat[0]=TAIKASANA&amp%3Bstatus[0]=1",
    kiinteistotahkola: "https://kiinteistotahkola.fi/fi/vuokra-asunnot-TAIKASANA/"
};

app.set('port', (process.env.PORT || 3002));


const details = {

};

app.get('/api/get_rent_data', async (req, res) => {

    const keys = Object.keys(urlMap);

    const { q } = req.query;

    if (cache.get(q)) {
        return res.status(200).send(cache.get(q));
    }

    let data;
    for (let i = 0; i < keys.length; i++) {
        const searchTerm = urlMap[keys[i]].replace('TAIKASANA', q);
        let rawData = await getUrl(searchTerm);
        data =  await parseResults(keys[i], rawData);

        cache.set(q, data);
        // remove
        break;
        //console.log(data);
        //res.status(200).send(data);
    }

    //console.log(data);
    res.charset = "utf-8";
    res.status(200).send(data);
});


const getUrl = async (url) => {

    console.log(url);
    return new Promise((resolve, reject) => {
       request.get({url, followAllRedirects: true, encoding: "binary"}, (err, res) => {
            if (err) reject(err);
            resolve(res.body);
       })
    });
};

/*
<div class="desc">
    <a tabindex="50" href="https://www.tori.fi/pohjois-pohjanmaa/Saunallinen_kalustettu_kaksio_Meritullissa_53693168.htm?ca=2&w=1">Saunallinen kalustettu kaksio Meritullissa</a>
<p class="list_price ineuros">660 &euro;/kk</p>
<p><a class="nohistory" onclick="return xt_click(this,'C','100','list_sis_finance','N');" href="https://www.tori.fi/asuntolaina.htm?id=53693168&link=list">Asuntolaina</a>
    <span class="pipe">|</span><a class="nohistory" onclick="return xt_click(this,'C','100','list_sis_insurance','N');"
href="https://www.tori.fi/kotivakuutus.htm?id=53693168&link=list">Kotivakuutus</a></p>
*/

const parseResults = async (service, data) => {

    switch (service) {
        case "tori":
            return await parseToriResults(data);
            break;
    }
};

const parseToriResults = async data => {
    const $ = cheerio.load(data);

    const $apartments = $('.desc > a')

    /*
    const item = {
        address: "",
        area: "",
        desc: "",
        rent: "",
        size: "",
        mod: "",
        link: ""
    };
    */

    const items = [];

    for (let i = 0; i < $apartments.length; i++) {
        if (!items[i]) items[i] = {};
        items[i].desc = $($apartments[i]).text().trim() || "";
        items[i].link = $($apartments[i]).attr('href') || "";
        const singleData = await parseToriItem(items[i].link);
        items[i].m2 = singleData.m2 || "";
        items[i].address = singleData.address || "";
        items[i].area = singleData.area || "";
        items[i].deposit = singleData.deposit || "";
    }
    /*
    await $apartments.each(async (i, elem) => {
        if (!items[i]) items[i] = {};
        items[i].desc = $(elem).text().trim() || "";
        items[i].link = $(elem).attr('href') || "";
        const singleData = await parseToriItem(items[i].link);
        items[i].m2 = singleData.m2 || "";
        items[i].address = singleData.address || "";
        items[i].area = singleData.area || "";
        items[i].deposit = singleData.deposit || "";
        //links.push($(this).text().trim());
    });
    */

    $('.list_price').each((i, elem) => {
        if (!items[i]) items[i] = {};
        items[i].rent = $(elem).text().trim() || "";
    });

    $('div.date_image').each((i, elem) => {
        if (!items[i]) items[i] = {};
        items[i].mod = $(elem).text().replace(/\s+/g, " ").trim() || "";
    });

    return { items };
};


const parseToriItem = async (link) => {

    const data = await getUrl(link);

    const $ = cheerio.load(data);


    const item = {};
    item.m2 = $('div.details > div.params > div > table > tbody > tr:nth-child(2) > td:nth-child(2)').text();
    item.address = $('div.details > div.params > div > table > tbody > tr:nth-child(6) > td:nth-child(5)').text();
    item.area = $('div.details > div.params > div > table > tbody > tr:nth-child(5) > td:nth-child(5)').text();
    item.deposit = $('div.details > div.params > div > table > tbody > tr:nth-child(4) > td:nth-child(5)').text();

    return item;
};


app.post('/api/q', (req, res) => {

});

app.listen(app.get('port'), function() {
    console.log('Server started on port ' + app.get('port'));
});